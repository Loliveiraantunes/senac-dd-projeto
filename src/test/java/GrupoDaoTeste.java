
import Projeto.grupoproduto.modulo.GrupoProdutoDAO;
import Projeto.revisao.aula.oo.model.GrupoProduto;
import Projeto.revisao.aula.oo.model.TipoProduto;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author loliv
 */
public class GrupoDaoTeste {
    
    public static void main(String[] args){
        
        GrupoProdutoDAO dao = new GrupoProdutoDAO();
        GrupoProduto grupoProduto = new GrupoProduto();
        
        grupoProduto.setNomeGrupoProduto("Batata");
        grupoProduto.setTipoProduto(TipoProduto.SERVICO);
        
//        dao.inserir(grupoProduto);
       grupoProduto.setNomeGrupoProduto("Teste");
       dao.alterar(grupoProduto);
      //  dao.excluir(grupoProduto.getIdGrupoProduto());
        for(GrupoProduto produto: dao.listarTodos()){
            System.out.println(produto.getIdGrupoProduto()+" - "+produto.getNomeGrupoProduto());
        }
        
        
        
    }
    
    
    
}
