/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componente;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 *
 * @author loliv
 */
public class Conexao {


public static Conexao conexao;    
    
    public static void main(String[] args) {
        
        Conexao conexao = Conexao.getInstance();
        conexao.getConnection();
        
    }
    
    
    public Connection getConnection(){
      
        Connection connection= null;
        
        Properties prop = new Properties();
        
        try {
            prop.load( new FileInputStream( new File("./src/main/resources/config.properties")));
        } catch (FileNotFoundException ex) {
            
            System.out.println("Deu pau, nennum arquivo encontrado");
            
        } catch (IOException ex) {
           System.out.println("Deu pau diferente");
        }
        
        
        String port = prop.getProperty("port");
        String server = prop.getProperty("server");
        String db = prop.getProperty("db");
        String user = prop.getProperty("user");
        String password = prop.getProperty("password");
        
     
        String url = "jdbc:mysql://"+server+":"+port+"/"+db;
        System.out.println(url);           
        try {
            connection = DriverManager.getConnection(url, user,password);
        } catch (Exception e) {
                throw new RuntimeException("Se Vira ae...");
        }
        
        return connection;
    }
    
    
    
    public static Conexao getInstance(){
        
        if(conexao == null){
            conexao = new Conexao();
        }
        return conexao;
    }
    
    
    
    
}
