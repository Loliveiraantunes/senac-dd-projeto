/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Componente.interfaces;

/**
 *
 * @author loliv
 */
public interface BaseDAO<A,B> {
    
    public A getPorId(B id);
    
    public boolean excluir(B id);
    
    public boolean alterar(A objeto);
    
    public void inserir(A obejto);
    
}
