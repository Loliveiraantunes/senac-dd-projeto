/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Projeto.grupoproduto.modulo;
import Projeto.revisao.aula.oo.model.GrupoProduto;
import Projeto.revisao.aula.oo.model.TipoProduto;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.ButtonGroup;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class CadGrupoProduto extends javax.swing.JDialog {

   public static void main (String[] args){
        CadGrupoProduto cadGrupoProduto = new CadGrupoProduto(5);
        cadGrupoProduto.setVisible(true);     
    }
   
   
    private JPanel pnBotoes;
    private JPanel pnTipoProduto;
    private Object java;
    private JPanel pnRadioTipo;
    private ButtonGroup rdGrupo;
    private GrupoProdutoDAO gpDAO = new GrupoProdutoDAO();
    private Integer idGrupoProduto;
    private JTextField txCodigo;
    private GrupoProduto grupoProduto;
    private JTextField txNome;
    private JRadioButton rdServico;
    private JRadioButton rdMercadoria;
    private JRadioButton rdMateriaPrima;  

    public CadGrupoProduto(Integer idGrupoProduto){
       
        configurarTela();
        configurarPanelTipoProduto();
        configurarPanelBotoes();
        configurarPanelDados();
        
        this.idGrupoProduto = idGrupoProduto;
        
        if (idGrupoProduto==null){
            grupoProduto = new GrupoProduto();
            return;
        }
        grupoProduto = gpDAO.getPorId(idGrupoProduto);
        txCodigo.setText(grupoProduto.getIdGrupoProduto().toString());
        txNome.setText(grupoProduto.getNomeGrupoProduto());
        if(grupoProduto.getTipoProduto()==TipoProduto.SERVICO){
            rdServico.setSelected(true);
        }
        if(grupoProduto.getTipoProduto()==TipoProduto.MATERIA_PRIMA){
            rdMateriaPrima.setSelected(true);
        }
        if(grupoProduto.getTipoProduto()==TipoProduto.MERCADORIA){
            rdMercadoria.setSelected(true);
        }
    }
    private void configurarTela(){
        this.setTitle("Cadastro");
        this.setSize(700,200);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setLayout(new BorderLayout());
        this.setLocationRelativeTo(null);
    }
    
    private void configurarPanelTipoProduto(){
        pnTipoProduto = new JPanel();
        pnTipoProduto.setLayout(new java.awt.GridLayout(3,2));
            JLabel lbCodigo = new JLabel("Código:");
        lbCodigo.setHorizontalAlignment(SwingConstants.LEFT);
            JLabel lbNome = new JLabel("Nome:");
        lbNome.setHorizontalAlignment(SwingConstants.LEFT);
            JLabel lbTipo = new JLabel("Tipo:");
        lbTipo.setHorizontalAlignment(SwingConstants.LEFT);
      
        txCodigo = new JTextField();
        txCodigo.setVisible(true);
        txCodigo.setHorizontalAlignment(SwingConstants.LEFT);
        txNome = new JTextField();
        txNome.setHorizontalAlignment(SwingConstants.LEFT);
        pnTipoProduto.add(lbCodigo);
        pnTipoProduto.add(txCodigo);
        pnTipoProduto.add(lbNome);
        pnTipoProduto.add(txNome);
        pnTipoProduto.add(lbTipo);
        
        add(pnTipoProduto);
    }
    private void configurarPanelBotoes(){
        pnBotoes = new JPanel();
        pnBotoes.setLayout(new java.awt.FlowLayout(FlowLayout.RIGHT));
        JButton btGravar = new JButton("Gravar");
        btGravar.setSize(new Dimension(80,30));
        JButton btCancelar = new JButton("Cancelar");
        btCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        
        btGravar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                JOptionPane.showMessageDialog(null, "Sucesso, Id:"+txCodigo.getText());
            
            }
        });
        
        
        btCancelar.setSize(new Dimension(80,30));
        pnBotoes.add(btGravar);
        pnBotoes.add(btCancelar);
        add(pnBotoes,BorderLayout.PAGE_END);        
    }
    private void configurarPanelDados(){        
        rdServico = new JRadioButton("Serviço");
        rdMercadoria = new JRadioButton("Mercadoria");
        rdMateriaPrima = new JRadioButton("Matéria Prima");  
        rdGrupo = new ButtonGroup();
        rdGrupo.add(rdServico);
        rdGrupo.add(rdMercadoria);
        rdGrupo.add(rdMateriaPrima);
        pnRadioTipo = new JPanel();
        pnRadioTipo.setLayout(new java.awt.FlowLayout(FlowLayout.LEFT));
        pnRadioTipo.add(rdServico);
        pnRadioTipo.add(rdMercadoria);
        pnRadioTipo.add(rdMateriaPrima);
        pnTipoProduto.add(pnRadioTipo);
    }      
}
