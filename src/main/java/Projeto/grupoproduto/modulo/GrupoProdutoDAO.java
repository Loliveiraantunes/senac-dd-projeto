/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Projeto.grupoproduto.modulo;

import Componente.Exceptions.CadastroException;
import Componente.Exceptions.InvalidValuesException;
import Componente.interfaces.BaseDAO;
import Projeto.revisao.aula.oo.model.GrupoProduto;
import Projeto.revisao.aula.oo.model.TipoProduto;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author loliv
 */
public class GrupoProdutoDAO implements BaseDAO< GrupoProduto, Integer> {
    private static ArrayList<GrupoProduto> listaGrupoProduto = new ArrayList<>();
    public GrupoProdutoDAO() { //construto
        if (listaGrupoProduto.size() > 0) {
            return;
        }
        listaGrupoProduto.add(new GrupoProduto(1, "Alimentos", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(2, "Estética", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(3, "Higiene", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(4, "Limpeza", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(5, "Cobre", TipoProduto.MATERIA_PRIMA));
        listaGrupoProduto.add(new GrupoProduto(6, "Mecânica", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(7, "Segurança", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(8, "Educação", TipoProduto.SERVICO));
        listaGrupoProduto.add(new GrupoProduto(9, "Automóveis", TipoProduto.MERCADORIA));
        listaGrupoProduto.add(new GrupoProduto(10, "Lã", TipoProduto.MATERIA_PRIMA));
        listaGrupoProduto.add(new GrupoProduto(11, "Algodão", TipoProduto.MATERIA_PRIMA));
    }

    @Override
    public GrupoProduto getPorId(Integer id) {
     
      if(!(id == 0) && !(id == null)){
        for(GrupoProduto produto: listaGrupoProduto){
            if(produto.getIdGrupoProduto().equals(id)){
            return produto;
            }
        }
        return null;   
      }else{
          
          throw new InvalidValuesException("Verifique se os Valores são válidos.");
          
      }  
       
    }

    @Override
    public boolean excluir(Integer id) {

        for(int i =0; i < listaGrupoProduto.size();i++){
            
            if(listaGrupoProduto.get(i).getIdGrupoProduto().equals(id)){
                listaGrupoProduto.remove(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean alterar(GrupoProduto objeto) {
        
       if(!(objeto == null) && !(objeto.getNomeGrupoProduto().equalsIgnoreCase(""))){
       
           for(int i = 0; i < listaGrupoProduto.size();i++){
               if(listaGrupoProduto.get(i).getIdGrupoProduto() == objeto.getIdGrupoProduto()){
               listaGrupoProduto.set(i, objeto);
               return true;
            }
        }
           return false;
       }else{
           throw new CadastroException();
       }
               
    }

    @Override
    public void inserir(GrupoProduto obejto) {
    
        if(obejto != null){
        int MenorId = 0;
        for (int i = 0; i < listaGrupoProduto.size(); i++) {
            if (listaGrupoProduto.get(i).getIdGrupoProduto() >= MenorId) {
                MenorId = listaGrupoProduto.get(i).getIdGrupoProduto();
            }
        }
        MenorId++;
        obejto.setIdGrupoProduto(MenorId);
        listaGrupoProduto.add(obejto);     
        } else throw new InvalidValuesException("Objeto Inválido");
       
    }

    public boolean equals(GrupoProduto produto) {
        return listaGrupoProduto.contains(produto);
    }

    
    public List<GrupoProduto> listarPorNome(String nome) {
        List<GrupoProduto> listaNome = new LinkedList<>();
        for (GrupoProduto grupProd : listaGrupoProduto) {
            if (nome != null
                    && (grupProd.getNomeGrupoProduto().toUpperCase().contains(nome.toUpperCase()))) {
                listaNome.add(grupProd);
            }
        }
        Collections.sort(listaNome, new Comparator<GrupoProduto>() {
            @Override
            public int compare(GrupoProduto gp1, GrupoProduto gp2) {
                int comp = gp1.getNomeGrupoProduto().compareToIgnoreCase(gp2.getNomeGrupoProduto());
                if (comp == 0) {
                    return gp1.getIdGrupoProduto().compareTo(gp2.getIdGrupoProduto());

                } else {
                    return comp;
                }
            }
        });
        return listaNome;
    }

    
    public List<GrupoProduto> listarPorTipo(TipoProduto tipoProduto) {
         List<GrupoProduto> produtos = new ArrayList<>();
        
       for(GrupoProduto produto : listaGrupoProduto){
           if(produto.getTipoProduto().equals(tipoProduto)){
               produtos.add(produto);
           }
       }
       return produtos;
    }

    public  List<GrupoProduto> listarTodos(){
        return listaGrupoProduto;
    }
    
}
