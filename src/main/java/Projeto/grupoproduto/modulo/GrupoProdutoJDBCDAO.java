/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Projeto.grupoproduto.modulo;

import Componente.Conexao;
import Componente.interfaces.BaseDAO;
import Projeto.revisao.aula.oo.model.GrupoProduto;
import Projeto.revisao.aula.oo.model.TipoProduto;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author loliv
 */
public class GrupoProdutoJDBCDAO implements BaseDAO< GrupoProduto, Integer> {

    public Conexao conexao = Conexao.getInstance();

    @Override
    public GrupoProduto getPorId(Integer id) {

        try {
            Statement stm = conexao.getConnection().createStatement();
            String query = "SELECT * FROM gruproproduto WHERE idgrupoproduto = " + id;
            ResultSet res = stm.executeQuery(query);
            if (!res.next()) {
                throw new RuntimeException("Não encontrado");
            }
            GrupoProduto gp = new GrupoProduto();

            gp.setNomeGrupoProduto(res.getString("nomegrupoProduto"));

            return gp;

        } catch (SQLException ex) {
            Logger.getLogger(GrupoProdutoJDBCDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public boolean excluir(Integer id) {

        return false;
    }

    @Override
    public boolean alterar(GrupoProduto objeto) {
        return true;
    }

    @Override
    public void inserir(GrupoProduto obejto) {

    }

    public boolean equals(GrupoProduto produto) {
        return true;
    }

    public List<GrupoProduto> listarPorNome(String nome) {
        return null;
    }

    public List<GrupoProduto> listarPorTipo(TipoProduto tipoProduto) {

        return null;
    }

    public List<GrupoProduto> listarTodos() {
        return null;
    }

}
