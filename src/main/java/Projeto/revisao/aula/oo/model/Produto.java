/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Projeto.revisao.aula.oo.model;

import java.util.Date;

/**
 *
 * @author loliv
 */
public abstract class Produto {
    
    private Long idProduto;
    private String nomeProduto;
    private TipoProduto tipoProduto;
    private String descricao;
    private Date dataCriacao;
    private Date dataAlteracao;
    private float perciCMS;
    private GrupoProduto grupoProduto;

    public abstract Long getIdProduto();

    public abstract void setIdProduto(Long idProduto);

    public abstract String getNomeProduto();

    public abstract void setNomeProduto(String nomeProduto);

    public abstract TipoProduto getTipoProduto();

    public abstract void setTipoProduto(TipoProduto tipoProduto) ;
    

    public abstract String getDescricao() ;

    public abstract void setDescricao(String descricao);
        
    public abstract  Date getDataCriacao();

    public abstract void setDataCriacao(Date dataCriacao);

    public abstract Date getDataAlteracao();
    
    public abstract void setDataAlteracao(Date dataAlteracao) ;

    public abstract float getPerciCMS();

    public abstract void setPerciCMS(float perciCMS);
    
    public abstract GrupoProduto getGrupoProduto();

    public abstract void setGrupoProduto(GrupoProduto grupoProduto);
    
     public abstract float getTotalPercImposto();
    
    
}
