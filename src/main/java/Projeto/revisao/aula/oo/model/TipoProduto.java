/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Projeto.revisao.aula.oo.model;

/**
 *
 * @author loliv
 */
public enum TipoProduto {
    
    MERCADORIA,
    SERVICO,
    MATERIA_PRIMA;
    
    private Integer id;

    public void TipoProduto(Integer id){
        this.id = id;
    }
    
    public Integer getId(){
        return id;
    }
    
}
